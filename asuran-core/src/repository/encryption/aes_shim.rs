use aes::cipher::StreamCipher;

use std::convert::TryInto;

/// This function performs AES256CTR using the fastest available implementation supported on the current machine, using runtime feature detection
pub fn aes_256_ctr(data: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
    use aes::{cipher::FromBlockCipher, Aes256, Aes256Ctr, NewBlockCipher};
    let block_cipher = Aes256::new_from_slice(key).unwrap();
    let mut stream_cihper = Aes256Ctr::from_block_cipher(block_cipher, iv.try_into().unwrap());
    let mut data = data.to_vec();
    stream_cihper.apply_keystream(&mut data);
    data
}
